This is an Ionic application that uses the openweathermap api to get weather and forecast information by current location. 

## Features
- Use 2 tabs:
  - Current Weather
  - Forecast (Show info for 5 days every 3 hours).
- Use OpenWeatherMap API.
- Get current location (Web browsers and mobile devices).
- Use default coordinates in case of location permission are disabled.

## Usage

### Settings

Add your openweathermap API_KEY in enviroment file (src/enviroments/enviroment)

```sh
$ cd src/enviroments/ - enviroment.prod.ts | enviroment.ts
...
$ apiKey: 'xxxxyyyyzzzz1234'
...
```

### Installation

##Benefeciaries 

The project's target audience includes anybody who wants to monitor weather conditions, such as normal people, farmers, commuters, people who like to travel, and companies/institutions that provide land, air, or sea transport.
Individuals – The application may benefit individuals by enabling them to know what weather conditions occur outside and plan accordingly in terms of clothing, items to carry, and/or the environment outdoors.
Farmers - This application will help farmers with crop planting and harvesting. Farmers will be able to determine the ideal time or season to harvest and/or plant based on the type of weather.
Commuters - The application could be useful for commuters such as workers and students. Commuters must be aware of the weather conditions for the day since commuting time is unpredictable; thus, weather monitoring can assist them in avoiding specific locations that may be affected by severe weather.
Travellers – Everyone wants to have the perfect trip or journey. Knowing the weather conditions may assist travellers in determining whether or not this particular day is suitable for travel. When a person wants to go to the beach, he or she anticipates bright weather. And, with a weather-monitoring app, travellers will be able to determine if the weather is in accordance with their plans; if not, they can simply change their plans.
Companies/Travel Companies – This type of application could be extremely beneficial to travel companies. When it comes to air travel, no one likes to fly during bad weather since it might result in a tragedy, and the same is applicable for sea and land travel.
