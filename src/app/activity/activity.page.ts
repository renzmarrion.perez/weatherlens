import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddNewTaskPage } from '../add-new-task/add-new-task.page';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.page.html',
  styleUrls: ['./activity.page.scss'],
})
export class ActivityPage implements OnInit {
  todoList=[]
  category =['Chores','Personal','Work','Shopping']
  taskName
  taskDate
  taskPriority
  taskCategory

  taskObject
  selectCategory(index){
    this.taskCategory = this.category[index]
  }
  addTasks(){
    this.taskObject = ({
      itemName:this.taskName,
      itemDueDate: this.taskDate,
      itemPriority:this.taskPriority,
      itemCategory:this.taskCategory
      
  
    })
    
    
    console.log(this.taskObject)
    this.todoList.push(this.taskObject)

    
    
    }
    delete(index){
      this.todoList.splice(index,1)
    }


  today: number = Date.now();
  constructor(public modalCtrl:ModalController) { }

//  async addTask(){
//   const modal = await this.modalCtrl.create({
//     component:AddNewTaskPage

    
    
//   })
//   modal.onDidDismiss().then(newTaskObject=>{
//     console.log(newTaskObject.data)
//     this.todoList.push(newTaskObject.data)
//   })
//   return await modal.present()
  
//  }






  ngOnInit() {
  }

}
