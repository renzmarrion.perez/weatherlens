# Ionic Weather App / WeatherLens

WeatherLens is an Ionic application that uses the OpenWeatherMap API to obtain current weather reports and other weather forecast information based on current location.

## Features
- Make use of OpenWeatherMap API.
- Determine (web browsers and mobile devices) current location.
- Use default coordinates if location permission is not enabled.
- Use the skeleton effect while the data is loaded.
- To reload data, use the "pull to refresh" effect.
- Use of custom image for the type of weather.
- Display the current location at the bottom of the main display (above tabs bar).
- Show error message when data could not be loaded.

## Usage


### Installation

Access to directory

```sh
$ cd weatherLens
```

Install the dependencies

```sh
$ npm install
```

### Serve

To serve in the browser

```sh
$ ionic serve
```

### To Add Platforms

```sh
$ ionic build
$ ionic capacitor add android
$ ionic capacitor add ios
```

### Run Platforms

```sh
$ npx cap open android
$ npx cap open ios
```

### Run test

```sh
$ npm test
```
